import React, {useState, useEffect} from 'react';

const App = () => {
  
  const [bolets, setBolets] = useState([]);
  const [nom, setNom] = useState('');
  const [color, setColor] = useState('');
  const api_url = 'http://localhost:8080/api';


  const cargaDatos = () => {
    fetch(api_url+'/bolets')
    .then( data => data.json())
    .then( datos => setBolets(datos))
    .catch( err => console.log(err));
  }

  useEffect(()=>{
    cargaDatos();
  }, []);

  const nouBolet= () =>{
    const bolet = {
      nom: nom,
      color: color
    };
    const jsonBolet = JSON.stringify(bolet);

    const opcionesFetch = {
      method: "POST",
      body: jsonBolet,
      headers: {'Content-Type': 'application/json'},
    }

    fetch(api_url+'/bolets', opcionesFetch)
    .then( resp => {
      setNom('');
      setColor('');
      cargaDatos();
    })
    .catch( err => console.log(err));
  }
  
  let lis = null;
  if (bolets.length>0){
    lis = bolets.map(el => <li key={el._id}>{el.nom}</li>)
  }

  return (
  <>
    <h1>Bolets!</h1>
    <ul>
      {lis}
    </ul>

    <br />
    Nom: <input type="text" value={nom} onChange={(e)=>setNom(e.target.value)} /><br />
    Color: <input type="text" value={color} onChange={(e)=>setColor(e.target.value)} /><br />
    <button onClick={nouBolet}>Crear</button>
    
  </>
);

}



export default App;
